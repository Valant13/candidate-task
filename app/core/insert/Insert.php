<?php

namespace App\Core\Insert;

class Insert
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function insert($search, $data, &$subject)
    {
        $pattern = '/' . preg_quote("[$search]", '/') . '/';
        $subject = preg_replace($pattern, $data, $subject, 1, $count);

        $result = boolval($count);
        return $result;
    }

    public function insertAll($search, $data, &$subject)
    {
        $subject = str_replace("[$search]", $data, $subject, $count);

        $result = boolval($count);
        return $result;
    }

    public function read($attributes, &$subject)
    {
        $pattern = "/{{";
        foreach ($attributes as $key => $attribute) {
            if ($key > 0) $pattern .= " ";
            $attribute = preg_quote($attribute, '/');
            $pattern .= "$attribute=\"(.*)\"";
        }
        $pattern .= "}}/";

        preg_match($pattern, $subject, $matches);
        $subject = preg_replace($pattern, '', $subject, 1);

        $structure = array();
        foreach ($attributes as $key => $attribute)
            $structure[$attribute] = $matches[$key + 1];

        return $structure;
    }

    public function readAll($attributes, &$subject)
    {
        $pattern = "/{{";
        foreach ($attributes as $key => $attribute) {
            if ($key > 0) $pattern .= " ";
            $attribute = preg_quote($attribute, '/');
            $pattern .= "$attribute=\"(.*)\"";
        }
        $pattern .= "}}/";

        preg_match_all($pattern, $subject, $matches);
        $subject = preg_replace($pattern, '', $subject);

        $structures = array();
        foreach ($attributes as $key => $attribute)
            $structures[$attribute] = $matches[$key + 1];

        return $structures;
    }
}