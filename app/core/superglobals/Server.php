<?php

namespace App\Core\Superglobals;

class Server extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_SERVER;
    }

    public static function set($key, $value)
    {
        $_SERVER[$key] = $value;
    }
}