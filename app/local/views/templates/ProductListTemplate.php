<script src="/js/ajax_manager.js"></script>
<script src="/js/product_list_table.js"></script>
<div id="product-list-container">
    <h1>Product list</h1>
    <div id="product-list-table-container"></div>

    <script>
        init();
        setData(JSON.parse('<?=$data?>'));
    </script>
</div>
