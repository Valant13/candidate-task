<script src="/js/ajax_manager.js"></script>
<script src="/js/product_details_likes.js"></script>
<div id="product-details-container">
    <h1><?= $name ?></h1>

    <h3><?= $sku ?></h3>

    <img src="/media/productimages/<?= $image ?>"/>

    <h3>
        Price
        <?php if ($discountPrice): ?>
            <span class="discount"><?= $discountPrice ?></span>
            <span class="old"><?= $price ?></span>
        <?php else: ?>
            <?= $price ?>
        <?php endif ?>
    </h3>

    <button id="product-details-likes-button">LIKE</button>
    <span id="product-details-likes-counter"><?= $likes ?></span>
    <script>
        init();
        setData(JSON.parse('<?=$data?>'));
    </script>

    <p><?= $description ?></p>

    <a href="/reviews/index?id=<?= $id ?>">Reviews</a>
</div>
