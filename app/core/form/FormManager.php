<?php

namespace App\Core\Form;

class FormManager
{
    protected static $forms = array();

    public static function getForm($formName): Form
    {
        if (!array_key_exists($formName, self::$forms)) {
            self::$forms[$formName] = new Form($formName);
        }
        return self::$forms[$formName];
    }
}