<?php

namespace App\Core\Superglobals;

class Get extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_GET;
    }
}