<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class ProductListModel extends Model
{
    public const listLimit = 100;

    public function getProducts($sortColumn, $sortOrder)
    {
        if ($sortColumn != 'name' && $sortColumn != 'price') return false;
        if ($sortOrder != 1 && $sortOrder != -1) return false;

        if ($sortColumn == 'name' && $sortOrder == 1) {
            return App::getTable('products')->selectProductsOrderedByNameAscLimited(self::listLimit);
        } elseif ($sortColumn == 'name' && $sortOrder == -1) {
            return App::getTable('products')->selectProductsOrderedByNameDescLimited(self::listLimit);
        } elseif ($sortColumn == 'price' && $sortOrder == 1) {
            return App::getTable('products')->selectProductsOrderedByPriceAscLimited(self::listLimit);
        } elseif ($sortColumn == 'price' && $sortOrder == -1) {
            return App::getTable('products')->selectProductsOrderedByPriceDescLimited(self::listLimit);
        }

        return false;
    }

    public function likeProduct($id)
    {
        return App::getTable('products')->updateLikesById($id);
    }

    public function validateReferer()
    {
        return App::getReferer()->validate();
    }
}