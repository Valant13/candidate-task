<?php

namespace App\Core\Log;

class Log
{
    protected const logDirectory = 'var/';
    protected const logFilename = 'system.log';
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function write($message, $filename = self::logFilename)
    {
        try {
            $date = date("Y-m-d H:i:s");

            $handle = fopen($this::logDirectory . $filename, "a");
            fwrite($handle, "[$date] $message\n");
            fclose($handle);
        } catch (\Exception $e) {
            throw new \Exception("Unable to write log. " . $e->getMessage());
        }
    }

    public function clear($filename = self::logFilename)
    {
        $handle = fopen($this::logDirectory . $filename, "w");
        fclose($handle);
    }
}