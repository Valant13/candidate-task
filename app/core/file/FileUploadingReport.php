<?php

namespace App\Core\File;

class FileUploadingReport
{
    public $fileUploadingMessage;
    public $fileUploadingDetails;
    public $fileUploadingSuccess;
}