<?php

namespace App\Core\Semaphore;

class Semaphore
{
    protected const max = 1;
    protected const permissions = 0666;
    protected const autoRelease = 1;
    private static $_instance = null;
    protected $handlers = array();

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function get($key, $max = self::max, $permissions = self::permissions, $autoRelease = self::autoRelease)
    {
        if (!is_string($key)) throw new \Exception("Semaphore key must be of type string");

        $keyHash = crc32($key);
        if (array_key_exists($keyHash, $this->handlers)) throw new \Exception("Semaphore with the same key has been registered");

        $semaphore = sem_get($keyHash, $max, $permissions, $autoRelease);
        if (!$semaphore) throw new \Exception("Semaphore registering failed");

        sem_acquire($semaphore);
        $this->handlers[$keyHash] = $semaphore;
    }

    public function release($key)
    {
        if (!is_string($key)) throw new \Exception("Semaphore key must be of type string");

        $keyHash = crc32($key);
        if (!array_key_exists($keyHash, $this->handlers)) throw new \Exception("Semaphore with this key has not been registered");

        $semaphore = $this->handlers[$keyHash];
        sem_release($semaphore);
    }

    public function remove($key)
    {
        if (!is_string($key)) throw new \Exception("Semaphore key must be of type string");

        $keyHash = crc32($key);
        if (!array_key_exists($keyHash, $this->handlers)) throw new \Exception("Semaphore with this key has not been registered");

        $semaphore = $this->handlers[$keyHash];
        sem_remove($semaphore);
    }
}