<?php

namespace App\Core\Application;

class ClassLoader
{
    public static function register()
    {
        spl_autoload_register(
            function ($className) {
                $filePath = self::createFilePath($className);
                self::includeFile($filePath);
            }
        );
    }

    protected static function createFilePath($className)
    {
        $directories = explode('\\', $className);
        $lastIndex = count($directories) - 1;
        for ($i = 0; $i < $lastIndex; ++$i)
            $directories[$i] = lcfirst($directories[$i]);

        $filePath = '';
        foreach ($directories as $item)
            $filePath .= DIRECTORY_SEPARATOR . $item;
        $filePath = substr($filePath, 1);
        $filePath .= '.php';
        return $filePath;
    }

    protected static function includeFile($filePath)
    {
        if (is_file($filePath))
            include_once $filePath;
        else
            throw new \Exception("File `$filePath` not found");
    }

    public static function getClassesFromDirectory($directory, $pattern = '*')
    {
        $classes = array();

        $subdirectories = self::getSubdirectoriesFromDirectory($directory);
        array_push($subdirectories, $directory);
        foreach ($subdirectories as $subdirectory) {
            $filePaths = glob($subdirectory . $pattern . '.php');
            foreach ($filePaths as $filePath) {
                self::includeFile($filePath);

                $class = self::createClassName($filePath);
                if (class_exists($class)) {
                    array_push($classes, $class);
                } else {
                    throw new \Exception("Class `$class` does not exist");
                }
            }
        }
        return $classes;
    }

    protected static function getSubdirectoriesFromDirectory($directory)
    {
        $subdirectories = glob($directory . '*', GLOB_ONLYDIR);
        foreach ($subdirectories as $key => $subdirectory) {
            $subdirectories[$key] = $subdirectory . '/';
            $subdirectories = array_merge($subdirectories, self::getSubdirectoriesFromDirectory($subdirectories[$key]));
        }
        return $subdirectories;
    }

    protected static function createClassName($filePath)
    {
        $directories = explode(DIRECTORY_SEPARATOR, $filePath);
        $lastIndex = count($directories) - 1;
        for ($i = 0; $i < $lastIndex; ++$i)
            $directories[$i] = ucfirst($directories[$i]);

        $className = '';
        foreach ($directories as $item)
            $className .= '\\' . $item;
        $className = substr($className, 1, -4);
        return $className;
    }
}