<div id="reviews-container">
    <h1><?= $name ?></h1>

    <?php foreach ($reviews as $review): ?>
        <div class="review">
            <h3><?= $review['date'] ?></h3>
            <h3><?= $review['name'] ?></h3>
            <p><?= $review['text'] ?></p>
        </div>
    <?php endforeach; ?>

    <form action="/reviews/add" method="POST" id="form">
        <input type="hidden" name="id" value="<?= $id ?>"><br>
        Name:<br>
        <input type="text" name="name" <?php if ($user): ?>value="<?= $user ?>"<?php endif; ?>><br>
        Review:<br>
        <input type="text" name="text" <?php if ($text): ?>value="<?= $text ?>"<?php endif; ?>><br>
        <input type="submit" value="SUBMIT">
    </form>
</div>
