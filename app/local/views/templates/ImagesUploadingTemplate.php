<div id="images-uploading-container">
    <h1>Images uploading report</h1>

    <?php if ($fileUploadingMessage): ?>
        <p><?= $fileUploadingMessage ?></p>
    <?php endif; ?>

    <?php if ($fileUploadingDetails): ?>
        <?php if (is_array($fileUploadingDetails)): ?>
            <?php foreach ($fileUploadingDetails as $item): ?>
                <p><?= $item ?></p>
            <?php endforeach; ?>
        <?php else: ?>
            <p><?= $fileUploadingDetails ?></p>
        <?php endif; ?>
    <?php endif; ?>
</div>
