<div id="products-uploading-container">
    <h1>Products uploading report</h1>

    <?php if ($fileReadingMessage): ?>
        <p><?= $fileReadingMessage ?></p>
    <?php endif; ?>

    <?php if ($productImportMessage): ?>
        <p><?= $productImportMessage ?></p>
    <?php endif; ?>

    <?php if ($productImportDetails): ?>
        <?php foreach ($productImportDetails as $item): ?>
            <p><?= $item ?></p>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
