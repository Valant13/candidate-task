<?php

namespace App;

use App\Core\Database\Database;
use App\Core\Database\Table;
use App\Core\Database\TableManager;
use App\Core\File\File;
use App\Core\Form\Form;
use App\Core\Form\FormManager;
use App\Core\Insert\Insert;
use App\Core\Log\Log;
use App\Core\Mvc\Model\Model;
use App\Core\Mvc\Model\ModelManager;
use App\Core\Mvc\View\Template;
use App\Core\Mvc\View\TemplateManager;
use App\Core\Mvc\View\View;
use App\Core\Referer\Referer;
use App\Core\Semaphore\Semaphore;
use App\Core\Xml\Xml;

class App
{
    public static function getModel($modelName): Model
    {
        return ModelManager::getModel($modelName);
    }

    public static function getTemplate($templateName): Template
    {
        return TemplateManager::getTemplate($templateName);
    }

    public static function getView(): View
    {
        return View::getInstance();
    }

    public static function getTable($tableName): Table
    {
        return TableManager::getTable($tableName);
    }

    public static function getDatabase(): Database
    {
        return Database::getInstance();
    }

    public static function getLog(): Log
    {
        return Log::getInstance();
    }

    public static function getFile(): File
    {
        return File::getInstance();
    }

    public static function getXml(): Xml
    {
        return Xml::getInstance();
    }

    public static function getSemaphore(): Semaphore
    {
        return Semaphore::getInstance();
    }

    public static function getForm($formName): Form
    {
        return FormManager::getForm($formName);
    }

    public static function getInsert(): Insert
    {
        return Insert::getInstance();
    }

    public static function getReferer(): Referer
    {
        return Referer::getInstance();
    }
}