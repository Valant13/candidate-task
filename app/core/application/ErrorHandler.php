<?php

namespace App\Core\Application;

class ErrorHandler
{
    public static function register()
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                if (!(error_reporting() & $errno)) {
                    return false;
                }

                $message = "";

                switch ($errno) {
                    case E_ERROR:
                        $message .= "Fatal error: ";
                        break;

                    case E_WARNING:
                        $message .= "Warning: ";
                        break;

                    case E_NOTICE:
                        $message .= "Notice: ";
                        break;

                    default:
                        $message .= "Unknown error: ";
                        break;
                }

                $message .= "$errstr in $errfile on line $errline";

                throw new \ErrorException($message);
            }
        );
    }
}