<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class DetailsModel extends Model
{
    public function getProduct($id)
    {
        return App::getTable('products')->selectProductById($id);
    }

    public function likeProduct($id)
    {
        return App::getTable('products')->updateLikesById($id);
    }

    public function validateReferer()
    {
        return App::getReferer()->validate();
    }
}