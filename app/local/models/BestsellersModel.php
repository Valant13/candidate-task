<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class BestsellersModel extends Model
{
    protected const bestsellersDirectory = 'var/';
    protected const bestsellersFileanchor = 'bestsellers.html';

    public function getBestsellersHTML()
    {
        $report = App::getFile()->read($this::bestsellersDirectory . $this::bestsellersFileanchor);
        if (!$report->fileReadingSuccess) throw new \Exception("Unable to read load bestsellers. " . $report->fileReadingMessage);

        $bestsellersHTML = $report->fileReadingContent;
        return $bestsellersHTML;
    }

    public function processBestsellersHTML(&$bestsellersHTML)
    {
        $structures = App::getInsert()->readAll(array('product_id', 'anchor', 'color'), $bestsellersHTML);

        foreach ($structures['product_id'] as $productId)
            App::getInsert()->insert('product PDP url', '/details/index?id=' . $productId, $bestsellersHTML);

        foreach ($structures['anchor'] as $key => $anchor) {
            $name = $this->getProductName($structures['product_id'][$key]);
            App::getInsert()->insert('Anchor', ($name ? $name : $anchor), $bestsellersHTML);
        }

        foreach ($structures['color'] as $color)
            App::getInsert()->insert('style', ($color ? "style=\"color: $color\"" : ""), $bestsellersHTML);
    }

    protected function getProductName($id)
    {
        return App::getTable('products')->selectNameById($id);
    }
}