<?php

namespace App\Core\Database;

use App\Core\Application\ClassLoader;

class Database
{
    protected const configDirectory = 'config/';
    protected const configFilename = 'database.php';
    protected const tableDirectory = 'app/local/tables/';
    private static $_instance = null;
    protected $handler;
    protected $name;
    protected $host;
    protected $user;
    protected $password;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function execute($query, $data = null)
    {
        $database = $this->getHandler();
        $statement = $database->prepare($query);
        $result = $statement->execute($data);
        if (!$result) throw new \Exception("Unable to execute PDO statement `$query`");
        return $statement;
    }

    protected function getHandler(): \PDO
    {
        if (!$this->handler)
            $this->connect();
        return $this->handler;
    }

    protected function connect()
    {
        $this->includeConfig();

        try {
            $this->handler = new \PDO('mysql:dbname=' . $this->name . ';host=' . $this->host, $this->user, $this->password);
        } catch (\PDOException $ex) {
            switch ($ex->getCode()) {
                case 1049:
                    throw new \Exception("Database not found");
                    break;
                case 1045:
                    throw new \Exception("Username or password is incorrect");
                    break;
                default:
                    throw new \PDOException($ex);
            }
        }
    }

    protected function includeConfig()
    {
        $configFile = $this::configDirectory . $this::configFilename;
        if (!is_file($configFile)) {
            throw new \Exception("File `$configFile` not found");
        }
        include_once $configFile;

        $this->name = DB_NAME;
        $this->host = DB_HOST;
        $this->user = DB_USER;
        $this->password = DB_PASSWORD;
    }

    public function quote($value, $type = \PDO::PARAM_STR)
    {
        if ($type == \PDO::PARAM_INT)
            return intval($value);
        return $this->getHandler()->quote($value, $type);
    }

    public function migrate(): DatabaseMigrationReport
    {
        $report = new DatabaseMigrationReport();

        try {
            $this->create();
            $database = $this->getHandler();
            $report->databaseConnectionMessage = "Database connection estimated successfully";
        } catch (\Exception $e) {
            $report->databaseConnectionMessage = "Database connection failed. " . $e->getMessage();
            return $report;
        }

        try {
            $this->configure();
            $report->databaseConfigurationMessage = "Database configured successfully";
        } catch (\Exception $e) {
            $report->databaseConfigurationMessage = "Database configuration failed. " . $e->getMessage();
            return $report;
        }

        try {
            $tableNames = ClassLoader::getClassesFromDirectory($this::tableDirectory, '*Table');
            $report->tablesCreationMessage = "Tables crated successfully";

            $tables = array();
            foreach ($tableNames as $tableName) {
                array_push($tables, new $tableName);
            }

            uasort($tables, function (Table $a, Table $b) {
                if (!is_int($a::getInstance()->migrationOrder)) throw new \Exception("Incorrect migration order of " . get_class($a));
                if (!is_int($b::getInstance()->migrationOrder)) throw new \Exception("Incorrect migration order of " . get_class($b));

                if ($a::getInstance()->migrationOrder == $b::getInstance()->migrationOrder)
                    return 0;
                return ($a::getInstance()->migrationOrder < $b::getInstance()->migrationOrder ? -1 : 1);
            });

            $report->tablesCreationDetails = array();
            $report->tablesCreationSuccess = array();
            foreach ($tables as $table) {
                try {
                    $table::getInstance()->create();
                    array_push($report->tablesCreationDetails, get_class($table) . " created");
                    $report->tablesCreationSuccess[get_class($table)] = true;
                } catch (\Exception $e) {
                    $report->tablesCreationMessage = "Creation of some tables failed";
                    array_push($report->tablesCreationDetails, get_class($table) . " failed. " . $e->getMessage());
                    $report->tablesCreationSuccess[get_class($table)] = false;
                }
            }
        } catch (\Exception $e) {
            $report->tablesCreationMessage = "Tables creation failed";
            $report->tablesCreationDetails = $e->getMessage();
            $report->tablesCreationSuccess = false;
        } finally {
            return $report;
        }
    }

    protected function create()
    {
        $this->includeConfig();

        $handler = new \PDO('mysql:host=' . $this->host, $this->user, $this->password);
        $handler->exec('DROP DATABASE IF EXISTS ' . $this->name);
        $handler->exec('CREATE DATABASE ' . $this->name);
    }

    protected function configure()
    {
        $database = $this->getHandler();
        $database->exec('ALTER DATABASE ' . $this->name . ' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;');
    }

    public function getLastInsertId()
    {
        return $this->getHandler()->lastInsertId();
    }
}