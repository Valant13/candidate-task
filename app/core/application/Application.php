<?php

namespace App\Core\Application;

use App\Core\Mvc\Route;
use App\Core\Superglobals\Session;

class Application
{
    public static function run()
    {
        ini_set('display_errors', 1);

        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");

        include_once 'ClassLoader.php';
        ClassLoader::register();

        ErrorHandler::register();

        Session::start();

        Route::run();
    }
}