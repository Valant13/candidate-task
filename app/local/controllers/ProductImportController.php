<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;
use App\Core\Mvc\Route;
use App\Core\Superglobals\Files;

class ProductImportController extends Controller
{
    public function indexAction()
    {
        App::getView()->setLayout('single');
        App::getView()->addTemplate('main', 'productImport');
        App::getView()->display();
    }

    public function uploadImagesAction()
    {
        if (!App::getModel('productImport')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Files::check('images')) {
            Route::redirect('/productImport');
        } else {
            App::getModel('productImport')->getSemaphore();
            $report = App::getModel('productImport')->uploadImages();

            $fileUploadingMessage = $report->fileUploadingMessage;
            App::getTemplate('imagesUploading')->addData('fileUploadingMessage', $fileUploadingMessage);

            $fileUploadingDetails = $report->fileUploadingDetails;
            App::getTemplate('imagesUploading')->addData('fileUploadingDetails', $fileUploadingDetails);

            App::getModel('productImport')->logImages($fileUploadingMessage, $fileUploadingDetails);
            App::getModel('productImport')->releaseSemaphore();

            App::getView()->setLayout('single');
            App::getView()->addTemplate('main', 'imagesUploading');
            App::getView()->display();
        }
    }

    public function uploadProductsAction()
    {
        if (!App::getModel('productImport')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Files::check('products')) {
            Route::redirect('/productImport');
        } else {
            App::getModel('productImport')->getSemaphore();
            $result = App::getModel('productImport')->uploadProducts();

            App::getTemplate('productsUploading')->addData('fileReadingMessage', $result['fileReadingMessage']);
            App::getTemplate('productsUploading')->addData('productImportMessage', $result['productImportMessage']);
            App::getTemplate('productsUploading')->addData('productImportDetails', $result['productImportDetails']);

            App::getModel('productImport')->logProducts(
                $result['fileReadingMessage'],
                $result['productImportMessage'],
                $result['productImportDetails']
            );
            App::getModel('productImport')->releaseSemaphore();

            App::getView()->setLayout('single');
            App::getView()->addTemplate('main', 'productsUploading');
            App::getView()->display();
        }
    }
}