<?php

namespace App\Core\Mvc\Model;

class ModelManager
{
    protected const modelNamespace = 'App\\Local\\Models';

    public static function getModel($modelName): Model
    {
        $modelName = self::modelNamespace . '\\' . ucfirst($modelName) . 'Model';
        $model = new $modelName();
        return $model::getInstance();
    }
}