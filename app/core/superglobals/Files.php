<?php

namespace App\Core\Superglobals;

class Files extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_FILES;
    }
}