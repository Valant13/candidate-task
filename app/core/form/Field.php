<?php

namespace App\Core\Form;

class Field
{
    public $content;
    public $pattern;

    public function __construct($content, $pattern)
    {
        $this->content = $content;
        $this->pattern = $pattern;
    }

    public function validate()
    {
        if (is_null($this->pattern)) throw new \Exception("Pattern is undefined");
        $result = preg_match($this->pattern, $this->content);
        return boolval($result);
    }
}