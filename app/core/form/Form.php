<?php

namespace App\Core\Form;

use App\Core\Superglobals\Session;

class Form
{
    protected $name;
    protected $fields = array();

    public function __construct($formName)
    {
        $this->name = $formName;
    }

    public function addField($name, $content, $pattern = null)
    {
        $this->fields[$name] = new Field($content, $pattern);
    }

    public function getField($name): Field
    {
        if (!array_key_exists($name, $this->fields)) throw new \Exception("Field `$name` is undefined");
        return $this->fields[$name];
    }

    public function removeField($name)
    {
        if (array_key_exists($name, $this->fields)) unset($this->fields[$name]);
    }

    public function validateField($name)
    {
        if (!array_key_exists($name, $this->fields)) throw new \Exception("Field `$name` is undefined");
        return $this->fields[$name]->validate();
    }

    public function saveToSession()
    {
        $data = array();
        foreach ($this->fields as $name => $field) {
            $data[$name]['content'] = $field->content;
            $data[$name]['pattern'] = $field->pattern;
        }
        Session::set($this->name, $data);
    }

    public function loadFromSession()
    {
        if (!Session::check($this->name)) return false;
        $this->fields = array();
        $data = Session::get($this->name);
        foreach ($data as $name => $field)
            $this->fields[$name] = new Field($field['content'], $field['pattern']);
        Session::remove($this->name);
        return true;
    }
}