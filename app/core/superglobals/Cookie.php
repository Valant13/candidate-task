<?php

namespace App\Core\Superglobals;

class Cookie extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_COOKIE;
    }

    public static function set($key, $value)
    {
        setcookie($key, $value);
    }
}