<?php

namespace App\Core\Superglobals;

class Post extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_POST;
    }
}