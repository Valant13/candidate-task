<?php

namespace App\Core\Database;

abstract class Table
{
    private static $_instance = array();
    public $migrationOrder = 0;

    public static function getInstance()
    {
        if (!array_key_exists(static::class, self::$_instance)) {
            self::$_instance[static::class] = new static();
        }
        return self::$_instance[static::class];
    }

    abstract public function create();
}