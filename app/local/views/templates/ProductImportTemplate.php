<div id="product-import-container">
    <h1>Product import</h1>

    <h3>Upload images</h3>
    <form enctype="multipart/form-data" action="/productImport/uploadImages" method="POST">
        <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
        <input name="images[]" type="file" accept=".png, .jpg, .jpeg" multiple/>
        <input type="submit" value="Upload file(s)"/>
    </form>

    <h3>Upload products</h3>
    <form enctype="multipart/form-data" action="/productImport/uploadProducts" method="POST">
        <input type="hidden" name="MAX_FILE_SIZE" value="10000"/>
        <input name="products" type="file" accept=".xml"/>
        <input type="submit" value="Upload file"/>
    </form>
</div>
