<?php

namespace App\Core\Mvc\View;

class Template
{
    protected $file;
    protected $data = array();

    public function __construct($templateFile)
    {
        $this->file = $templateFile;
    }

    public function addData($key, $value)
    {
        $this->data[$key] = $this->quote($value);
    }

    protected function quote($data)
    {
        if (is_array($data))
            foreach ($data as $key => $value)
                $data[$key] = $this->quote($value);
        else
            $data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');

        return $data;
    }

    public function removeData($key)
    {
        if (array_key_exists($key, $this->data)) unset($this->data[$key]);
    }

    public function addSafeData($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function addJsonData($key, $value)
    {
        $this->data[$key] = json_encode($this->quote($value), JSON_UNESCAPED_UNICODE);
    }

    public function display()
    {
        extract($this->data);

        if (!is_file($this->file)) {
            throw new \Exception("File `" . $this->file . "` not found");
        }
        ob_start();
        include $this->file;
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}