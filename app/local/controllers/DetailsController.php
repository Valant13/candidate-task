<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;
use App\Core\Mvc\Route;
use App\Core\Superglobals\Get;
use App\Core\Superglobals\Post;

class DetailsController extends Controller
{
    public function indexAction()
    {
        if (!Get::check('id')) {
            Route::redirectError404();
        } else {
            $id = Get::get('id');

            $product = App::getModel('details')->getProduct($id);
            if (!$product) Route::redirectError404();

            App::getTemplate('details')->addData('id', $id);
            App::getTemplate('details')->addData('name', $product['name']);
            App::getTemplate('details')->addData('sku', $product['sku']);
            App::getTemplate('details')->addData('image', $product['image']);
            App::getTemplate('details')->addData('description', $product['description']);
            App::getTemplate('details')->addData('price', $product['price']);
            App::getTemplate('details')->addData('discountPrice', $product['discount_price']);
            App::getTemplate('details')->addData('likes', $product['likes']);

            $data = array();
            $data['state'] = 'success';
            $data['likes'] = $product['likes'];
            $data['id'] = $id;

            App::getTemplate('details')->addJsonData('data', $data);

            App::getView()->setLayout('single');
            App::getView()->addTemplate('main', 'details');
            App::getView()->display();
        }
    }

    public function likeAction()
    {
        if (!App::getModel('details')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Post::check('id')) {
            App::getView()->answerAjax(array('state' => 'error'));
        } else {
            $id = Post::get('id');
            $success = App::getModel('details')->likeProduct($id);

            $data = array();
            if ($success)
                $data['state'] = 'success';
            else
                $data['state'] = 'error';
            App::getView()->answerAjax($data);
        }
    }
}