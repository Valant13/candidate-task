var container;

function init() {
    container = document.getElementById('product-list-table-container');
    container.addEventListener('click', click);
}

function click(event) {
    var target = event.target;
    var params = target.id.split('-');

    if (params[0] === 'row' && params[1] === 'header' && params[2] === 'name') {
        var data = [];
        data['sort_column'] = 'name';
        data['sort_order'] = (sortColumn === 'name' ? -sortOrder : 1);
        requestAjax(data, '/home/sort', setData);
    }

    if (params[0] === 'row' && params[1] === 'header' && params[2] === 'price') {
        var data = [];
        data['sort_column'] = 'price';
        data['sort_order'] = (sortColumn === 'price' ? -sortOrder : 1);
        requestAjax(data, '/home/sort', setData);
    }

    if (params[0] === 'row' && params[1] === 'likes' && params[2] === 'button') {
        var key = params[3];
        var id = products[key]['id_product'];

        if (!isLiked[id]) {
            target.classList.add('pressed');
            products[key]['likes']++;
            isLiked[id] = true;
            draw();
            var data = [];
            data['id'] = id;
            requestAjax(data, '/home/like', function (data) {
            });
        }
    }
}

var products;
var sortColumn;
var sortOrder;
var isLiked = [];

function setData(data) {
    console.log(data);
    if (data['state'] === 'success') {
        sortColumn = data['sort_column'];
        sortOrder = data['sort_order'];
        products = data['products'];
        draw();
    }
}

function sortSign(column) {
    if (sortColumn === column) {
        if (sortOrder == 1) return '▲';
        return '▼';
    }
    return '';
}

function getRow(key, id, name, price, likes) {
    var row = document.createElement('tr');
    row.setAttribute('id', 'product-list-table-row-' + key);
    row.classList.add('table-row');
    row.innerHTML =
        '<td><a href="/details/index?id=' + id + '">' + name + '</a></td>' +
        '<td>' + price + '</td>' +
        '<td><span id="row-likes-counter-' + key + '">' + likes + '</span></td>' +
        '<td><button id="row-likes-button-' + key + '">LIKE</button></td>'
    ;

    return row;
}

function getDiscountRow(key, id, name, price, discountPrice, likes) {
    var row = document.createElement('tr');
    row.setAttribute('id', 'product-list-table-row-' + id);
    row.classList.add('table-row');
    row.innerHTML =
        '<td><a href="/details/index?id=' + id + '">' + name + '</a></td>' +
        '<td><span class="discount">' + discountPrice + '</span> <span class="old">' + price + '</span></td>' +
        '<td><span id="row-likes-counter-' + key + '">' + likes + '</span></td>' +
        '<td><button id="row-likes-button-' + key + '">LIKE</button></td>'
    ;

    return row;
}

function getHeaderRow() {
    var row = document.createElement('tr');
    row.setAttribute('id', 'product-list-table-row-header');
    row.classList.add('table-row', 'header');
    row.innerHTML =
        '<th id="row-header-name">Name ' + sortSign('name') + '</th>' +
        '<th id="row-header-price">Price ' + sortSign('price') + '</th>' +
        '<th id="row-header-likes-counter">Likes</th>' +
        '<th id="row-header-likes-button"></th>'
    ;

    return row;
}

function draw() {
    var table = document.getElementById('product-list-table');
    if (table) table.remove();
    table = document.createElement('table');
    table.setAttribute('id', 'product-list-table');
    table.classList.add('product-list-table');

    var row = getHeaderRow();
    table.appendChild(row);

    for (var key in products) {
        if (products.hasOwnProperty(key)) {
            if (products[key]['discount_price']) {
                row = getDiscountRow(
                    key,
                    products[key]['id_product'],
                    products[key]['name'],
                    products[key]['price'],
                    products[key]['discount_price'],
                    products[key]['likes']
                );
                table.appendChild(row);
            }
            else {
                row = getRow(
                    key,
                    products[key]['id_product'],
                    products[key]['name'],
                    products[key]['price'],
                    products[key]['likes']
                );
                table.appendChild(row);
            }
        }
    }

    container.appendChild(table);
}