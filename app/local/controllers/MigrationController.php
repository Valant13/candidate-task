<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;
use App\Core\Mvc\Route;

class MigrationController extends Controller
{
    public function indexAction()
    {
        if (!App::getModel('migration')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        $report = App::getModel('migration')->migrateDatabase();

        $databaseConnectionMessage = $report->databaseConnectionMessage;
        App::getTemplate('migration')->addData('databaseConnectionMessage', $databaseConnectionMessage);

        $databaseConfigurationMessage = $report->databaseConfigurationMessage;
        App::getTemplate('migration')->addData('databaseConfigurationMessage', $databaseConfigurationMessage);

        $tablesCreationMessage = $report->tablesCreationMessage;
        App::getTemplate('migration')->addData('tablesCreationMessage', $tablesCreationMessage);

        $tablesCreationDetails = $report->tablesCreationDetails;
        App::getTemplate('migration')->addData('tablesCreationDetails', $tablesCreationDetails);

        App::getModel('migration')->logMigration(
            $databaseConnectionMessage,
            $databaseConfigurationMessage,
            $tablesCreationMessage,
            $tablesCreationDetails
        );

        App::getView()->setLayout('single');
        App::getView()->addTemplate('main', 'migration');
        App::getView()->display();
    }
}