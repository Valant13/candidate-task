<?php

namespace App\Core\Referer;

use App\Core\Superglobals\Server;

class Referer
{
    protected const configDirectory = 'config/';
    protected const configFilename = 'referer.php';
    private static $_instance = null;
    protected $enable;
    protected $whitelist;

    protected function __construct()
    {
        $this->includeConfig();
    }

    protected function includeConfig()
    {
        $configFile = $this::configDirectory . $this::configFilename;
        if (!is_file($configFile)) {
            throw new \Exception("File `$configFile` not found");
        }
        include_once $configFile;

        $this->enable = REF_ENABLE;
        $this->whitelist = REF_WHITELIST;
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function validate()
    {
        if (!$this->enable) return true;

        $referer = Server::get('HTTP_REFERER');

        if (!$referer) return true;

        foreach ($this->whitelist as $host) {
            $pattern = '/' . preg_quote($host, '/') . '.*/';
            $result = preg_match($pattern, $referer);
            if ($result) return true;
        }
        return false;
    }
}