function requestAjax(data, url, func) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', url, true);
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = this.responseText;

            try {
                var answer = JSON.parse(response);
                func(answer);
            }
            catch (err) {
                console.log(err);
            }
        }
    };
    var formData = new FormData();
    for (var key in data)
        if (data.hasOwnProperty(key))
            formData.append(key, data[key]);
    xmlhttp.send(formData);
}