var counter;
var button;

function init() {
    counter = document.getElementById('product-details-likes-counter');
    button = document.getElementById('product-details-likes-button');
    button.addEventListener('click', click);
}

function click(event) {
    if (!isLiked) {
        likes++;
        isLiked = true;
        draw();
        var data = [];
        data['id'] = id;
        requestAjax(data, '/details/like', function (data) {
        });
    }
}

var likes;
var isLiked = false;
var id;

function setData(data) {
    if (data['state'] === 'success') {
        likes = data['likes'];
        id = data['id'];
        draw();
    }
}

function draw() {
    counter.innerHTML = likes;
    if (isLiked)
        counter.classList.add('liked');
}