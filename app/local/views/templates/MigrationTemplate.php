<div id="migration-container">
    <h1>Migration</h1>

    <?php if ($databaseConnectionMessage): ?>
        <p><?= $databaseConnectionMessage ?></p>
    <?php endif; ?>

    <?php if ($databaseConfigurationMessage): ?>
        <p><?= $databaseConfigurationMessage ?></p>
    <?php endif; ?>

    <?php if ($tablesCreationMessage): ?>
        <p><?= $tablesCreationMessage ?></p>
    <?php endif; ?>

    <?php if ($tablesCreationDetails): ?>
        <?php if (is_array($tablesCreationDetails)): ?>
            <?php foreach ($tablesCreationDetails as $item): ?>
                <p><?= $item ?></p>
            <?php endforeach; ?>
        <?php else: ?>
            <p><?= $tablesCreationDetails ?></p>
        <?php endif; ?>
    <?php endif; ?>
</div>
