<?php

namespace App\Local\Tables;

use App\App;
use App\Core\Database\Table;

class ReviewsTable extends Table
{
    public $migrationOrder = 1;

    public function create()
    {
        App::getDatabase()->execute("
            CREATE TABLE reviews (
                id_review INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                id_product INT NOT NULL, 
                date DATETIME, 
                name VARCHAR(32), 
                text TEXT
            );
            
            CREATE INDEX date ON reviews(date);
            
            ALTER TABLE reviews ADD INDEX (id_product);
            
            ALTER TABLE reviews 
            ADD CONSTRAINT reviews_id_product 
            FOREIGN KEY (id_product)
            REFERENCES products(id_product) 
            ON UPDATE CASCADE 
            ON DELETE CASCADE;
        ");
    }

    public function selectReviewsByProductId($productId)
    {
        $statement = App::getDatabase()->execute("SELECT date, name, text FROM reviews WHERE id_product = ? ORDER BY date DESC;", array($productId));

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function insertReviewWithDateNow($productId, $name, $text)
    {
        $statement = App::getDatabase()->execute("INSERT INTO reviews VALUES(NULL, ?, NOW(), ?, ?);", array($productId, $name, $text));

        $result = boolval($statement->rowCount());
        return $result;
    }

    public function insertReview($productId, $date, $name, $text)
    {
        $statement = App::getDatabase()->execute("INSERT INTO reviews VALUES(NULL, ?, ?, ?, ?);", array($productId, $date, $name, $text));

        $result = boolval($statement->rowCount());
        return $result;
    }

    public function deleteReviewsByProductId($id)
    {
        $statement = App::getDatabase()->execute("DELETE FROM reviews WHERE id_product = ?", array($id));

        $result = boolval($statement->rowCount());
        return $result;
    }
}