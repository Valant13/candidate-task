<?php

namespace App\Local\Tables;

use App\App;
use App\Core\Database\Table;

class DiscountsTable extends Table
{
    public $migrationOrder = 1;

    public function create()
    {
        App::getDatabase()->execute("
            CREATE TABLE discounts (
                id_discount INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                id_product INT NOT NULL, 
                starts_at DATETIME, 
                ends_at DATETIME, 
                CHECK(starts_at < ends_at),
                percent TINYINT, 
                CHECK(percent > 0 AND percent <= 100)
            );
            
            ALTER TABLE discounts ADD INDEX (id_product);
            
            ALTER TABLE discounts 
            ADD CONSTRAINT discounts_id_product 
            FOREIGN KEY (id_product)
            REFERENCES products(id_product) 
            ON UPDATE CASCADE 
            ON DELETE CASCADE;
        ");
    }

    public function insertDiscount($productId, $startsAt, $endsAt, $percent)
    {
        $statement = App::getDatabase()->execute("INSERT INTO discounts VALUES(NULL, ?, ?, ?, ?);", array($productId, $startsAt, $endsAt, $percent));

        $result = boolval($statement->rowCount());
        return $result;
    }

    public function deleteDiscountsByProductId($id)
    {
        $statement = App::getDatabase()->execute("DELETE FROM discounts WHERE id_product = ?", array($id));

        $result = boolval($statement->rowCount());
        return $result;
    }
}