<?php

namespace App\Core\Mvc;

use App\Core\Log\Log;
use App\Core\Superglobals\Server;

class Route
{
    protected const controllerNamespace = 'App\\Local\\Controllers';

    public static function run()
    {
        $controllerName = 'Home';
        $actionName = 'index';

        $uri = Server::get('REQUEST_URI');

        $routes = explode('/', explode('?', $uri)[0]);

        if (!empty($routes[1]))
            $controllerName = $routes[1];
        $controllerName = self::controllerNamespace . '\\' . ucfirst($controllerName) . 'Controller';
        try {
            $controller = new $controllerName();
        } catch (\Exception $e) {
            self::redirectError404();
        }


        if (!empty($routes[2]))
            $actionName = $routes[2];
        $actionName = $actionName . 'Action';
        $action = $actionName;

        if (method_exists($controller, $action)) {
            try {
                $controller::getInstance()->$action();
            } catch (\Exception $e) {
                Log::getInstance()->write($e->getMessage());
                self::redirectError500();
            }
        } else {
            self::redirectError404();
        }
    }

    public static function redirectError404()
    {
        header("Location: /error404");
    }

    public static function redirectError500()
    {
        header("Location: /error500");
    }

    public static function redirect($url)
    {
        header("Location: $url");
    }
}