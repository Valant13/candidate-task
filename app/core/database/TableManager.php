<?php

namespace App\Core\Database;

class TableManager
{
    protected const tableNamespace = 'App\\Local\\Tables';

    public static function getTable($tableName): Table
    {
        $tableName = self::tableNamespace . '\\' . ucfirst($tableName) . 'Table';
        $table = new $tableName();
        return $table->getInstance();
    }
}