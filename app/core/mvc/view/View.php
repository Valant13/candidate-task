<?php

namespace App\Core\Mvc\View;

class View
{
    protected const layoutDirectory = 'app/local/views/layouts/';
    private static $_instance = null;
    protected $templates = array();
    protected $layout;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function addTemplate($position, $templateName)
    {
        $this->templates[$position] = $templateName;
    }

    public function removeTemplate($position)
    {
        if (array_key_exists($position, $this->templates)) unset($this->templates[$position]);
    }

    public function setLayout($layoutName)
    {
        $layoutFile = $this::layoutDirectory . ucfirst($layoutName) . 'Layout' . '.php';
        if (!is_file($layoutFile)) {
            throw new \Exception("File `$layoutFile` not found");
        }
        $this->layout = $layoutFile;
    }

    public function removeLayout()
    {
        $this->layout = null;
    }

    public function display()
    {
        if ($this->layout) {
            $templatesContent = array();
            foreach ($this->templates as $position => $templateName) {
                $templatesContent[$position] = TemplateManager::getTemplate($templateName)->display();
            }
            extract($templatesContent);

            if (!is_file($this->layout)) {
                throw new \Exception("File `" . $this->layout . "` not found");
            }
            ob_start();
            include $this->layout;
            $content = ob_get_contents();
            ob_end_clean();

            echo $content;
        } else {
            $content = null;

            foreach ($this->templates as $templateName) {
                $content .= TemplateManager::getTemplate($templateName)->display();
            }

            echo $content;
        }
    }

    public function answerAjax($data)
    {
        $content = json_encode($this->quote($data), JSON_UNESCAPED_UNICODE);

        echo $content;
    }

    protected function quote($data)
    {
        if (is_array($data))
            foreach ($data as $key => $value)
                $data[$key] = $this->quote($value);
        else
            $data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');

        return $data;
    }
}