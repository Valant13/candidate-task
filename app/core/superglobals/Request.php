<?php

namespace App\Core\Superglobals;

class Request extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_REQUEST;
    }
}