<?php

namespace App\Core\Superglobals;

class Session extends ArrayLogic
{
    public static function register()
    {
        self::$array = &$_SESSION;
    }

    public static function start()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    public static function destroy()
    {
        if (session_status() == PHP_SESSION_ACTIVE)
            session_destroy();
    }

    public static function set($key, $value)
    {
        if (session_status() == PHP_SESSION_ACTIVE)
            $_SESSION[$key] = $value;
    }

    public static function remove($key)
    {
        if (session_status() == PHP_SESSION_ACTIVE)
            unset($_SESSION[$key]);
    }
}