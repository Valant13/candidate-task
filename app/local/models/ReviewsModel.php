<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class ReviewsModel extends Model
{
    public function getProductName($id)
    {
        return App::getTable('products')->selectNameById($id);
    }

    public function doesProductExist($id)
    {
        return App::getTable('products')->selectExistsId($id);
    }

    public function getReviews($productId)
    {
        return App::getTable('reviews')->selectReviewsByProductId($productId);
    }

    public function addReview($productId, $name, $text)
    {
        return App::getTable('reviews')->insertReviewWithDateNow($productId, $name, $text);
    }

    public function getFormFieldsContent(&$name, &$text)
    {
        $name = null;
        $text = null;
        if (App::getForm('reviews')->loadFromSession()) {
            $name = App::getForm('reviews')->getField('name')->content;
            $text = App::getForm('reviews')->getField('text')->content;
        }
    }

    public function validateForm($name, $text)
    {
        App::getForm('reviews')->addField('name', $name, "/.{1,32}/");
        App::getForm('reviews')->addField('text', $text, "/.{1,4096}/");

        if (!App::getForm('reviews')->validateField('name') || !App::getForm('reviews')->validateField('text')) {
            App::getForm('reviews')->saveToSession();
            return false;
        }

        return true;
    }

    public function validateReferer()
    {
        return App::getReferer()->validate();
    }
}