<?php

namespace App\Core\Superglobals;

abstract class ArrayLogic
{
    public static $array;

    public static function checkMultiple($keys)
    {
        static::register();

        $result = true;
        foreach ($keys as $key) {
            if (!isset(static::$array[$key])) {
                $result = false;
                break;
            }
        }
        return $result;
    }

    public static abstract function register();

    public static function check($key)
    {
        static::register();

        return isset(static::$array[$key]);
    }

    public static function getMultiple($keys)
    {
        static::register();

        $result = array();
        foreach ($keys as $key) {
            if (isset(static::$array[$key])) $result[$key] = self::$array[$key];
            else {
                $result = false;
                break;
            }
        }
        return $result;
    }

    public static function get($key)
    {
        static::register();

        if (isset(static::$array[$key])) return static::$array[$key];
        return false;
    }
}