<?php

namespace App\Core\Xml;

class Xml
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function parse($xml)
    {
        return simplexml_load_string($xml);
    }

    public function validate($xml, $xsd = null)
    {
        $doc = new \DOMDocument();

        try {
            $doc->loadXML($xml);
            $error = null;
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $error = substr($error, 32, -63);
            return $error;
        }

        if (!is_null($xsd)) {
            try {
                $doc->schemaValidateSource($xsd);
                $error = null;
            } catch (\Exception $e) {
                $error = $e->getMessage();
                $error = substr($error, 46, -63);
                return $error;
            }
        }

        return $error;
    }

    public function split($xml)
    {
        $nodes = simplexml_load_string($xml);
        $xmlArray = array();

        foreach ($nodes as $node)
            array_push($xmlArray, $node->asXML());
        return $xmlArray;
    }
}