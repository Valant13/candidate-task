<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;
use App\Core\Mvc\Route;
use App\Core\Superglobals\Get;
use App\Core\Superglobals\Post;

class ReviewsController extends Controller
{
    public function indexAction()
    {
        if (!Get::check('id')) {
            Route::redirectError404();
        } else {
            $id = Get::get('id');

            $exists = App::getModel('reviews')->doesProductExist($id);
            if (!$exists) {
                Route::redirectError404();
                return;
            }

            $name = App::getModel('reviews')->getProductName($id);
            $reviews = App::getModel('reviews')->getReviews($id);

            $user = null;
            $text = null;
            App::getModel('reviews')->getFormFieldsContent($user, $text);

            App::getTemplate('reviews')->addData('id', $id);
            App::getTemplate('reviews')->addData('name', $name);
            App::getTemplate('reviews')->addData('reviews', $reviews);

            App::getTemplate('reviews')->addData('user', $user);
            App::getTemplate('reviews')->addData('text', $text);

            App::getView()->setLayout('single');
            App::getView()->addTemplate('main', 'reviews');
            App::getView()->display();
        }
    }

    public function addAction()
    {
        if (!App::getModel('reviews')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Post::check('id')) {
            Route::redirectError404();
        } elseif (!Post::checkMultiple(['name', 'text'])) {
            $id = Post::get('id');
            Route::redirect('/reviews/index?id=' . $id);
        } else {
            $id = Post::get('id');
            $user = Post::get('name');
            $text = Post::get('text');

            $exists = App::getModel('reviews')->doesProductExist($id);
            if (!$exists) {
                Route::redirectError404();
                return;
            }

            if (!App::getModel('reviews')->validateForm($user, $text)) {
                Route::redirect('/reviews/index?id=' . $id . '#form');
                return;
            }

            App::getModel('reviews')->addReview($id, $user, $text);

            Route::redirect('/reviews/index?id=' . $id);

        }
    }
}