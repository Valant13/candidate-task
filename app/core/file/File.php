<?php

namespace App\Core\File;

use App\Core\Superglobals\Files;

class File
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    public function uploadToDirectory($key, $directory, $uploadConfirmFunc): FileUploadingReport
    {
        $report = new FileUploadingReport();
        if (is_array(Files::get($key)['name'])) {
            $report->fileUploadingMessage = "Files uploaded successfully";
            $report->fileUploadingDetails = array();

            try {
                $fileKeys = array_keys(Files::get($key)['name']);
                foreach ($fileKeys as $fileKey) {
                    $name = basename(Files::get($key)['name'][$fileKey]);
                    $type = Files::get($key)['type'][$fileKey];
                    $size = Files::get($key)['size'][$fileKey];
                    $tmpName = Files::get($key)['tmp_name'][$fileKey];
                    $error = Files::get($key)['error'][$fileKey];

                    try {
                        $confirm = $this->checkFile($name, $type, $size, $tmpName, $error, $uploadConfirmFunc);
                        if (!$confirm) throw new \Exception("Uploading was not confirmed");
                        move_uploaded_file($tmpName, $directory . $name);
                        array_push($report->fileUploadingDetails, "$name uploaded");
                    } catch (\Exception $e) {
                        $report->fileUploadingMessage = "Uploading of some files failed";
                        array_push($report->fileUploadingDetails, "$name failed. " . $e->getMessage());
                    }
                }
            } catch (\Exception $e) {
                $report->fileUploadingMessage = "Files uploading failed";
                $report->fileUploadingDetails = $e->getMessage();
            } finally {
                return $report;
            }
        } else {
            $report->fileUploadingMessage = "File uploaded successfully";
            $report->fileUploadingDetails;

            $name = basename(Files::get($key)['name']);
            $type = Files::get($key)['type'];
            $size = Files::get($key)['size'];
            $tmpName = Files::get($key)['tmp_name'];
            $error = Files::get($key)['error'];

            try {
                $confirm = $this->checkFile($name, $type, $size, $tmpName, $error, $uploadConfirmFunc);
                if (!$confirm) throw new \Exception("Uploading was not confirmed");
                move_uploaded_file($tmpName, $directory . $name);
                $report->fileUploadingDetails = "$name uploaded";
            } catch (\Exception $e) {
                $report->fileUploadingMessage = "File uploading failed";
                $report->fileUploadingDetails = "$name failed. " . $e->getMessage();
            } finally {
                return $report;
            }
        }
    }

    protected function checkFile($name, $type, $size, $tmpName, $error, $uploadConfirmFunc)
    {
        if ($error != UPLOAD_ERR_OK) throw new \Exception("Error: $error");

        if (!is_uploaded_file($tmpName)) throw new \Exception("File was not uploaded by method POST");

        $confirm = $uploadConfirmFunc($name, $type, $size, $tmpName);

        return $confirm;
    }

    public function uploadAndRead($key, $uploadConfirmFunc): FileReadingReport
    {
        $report = new FileReadingReport();
        try {
            if (is_array(Files::get($key)['name'])) throw new \Exception("Unable to read multiple files");

            $name = basename(Files::get($key)['name']);
            $type = Files::get($key)['type'];
            $size = Files::get($key)['size'];
            $tmpName = Files::get($key)['tmp_name'];
            $error = Files::get($key)['error'];

            $confirm = $this->checkFile($name, $type, $size, $tmpName, $error, $uploadConfirmFunc);
            if (!$confirm) throw new \Exception("Uploading was not confirmed");

            $content = file_get_contents($tmpName);

            $report->fileReadingMessage = "File read successfully";
            $report->fileReadingSuccess = true;
            $report->fileReadingContent = $content;
        } catch (\Exception $e) {
            $report->fileReadingMessage = "File reading failed. " . $e->getMessage();
            $report->fileReadingSuccess = false;
        } finally {
            return $report;
        }
    }

    public function read($file): FileReadingReport
    {
        $report = new FileReadingReport();
        try {
            $content = file_get_contents($file);

            $report->fileReadingMessage = "File read successfully";
            $report->fileReadingSuccess = true;
            $report->fileReadingContent = $content;
        } catch (\Exception $e) {
            $report->fileReadingMessage = "File reading failed. " . $e->getMessage();
            $report->fileReadingSuccess = false;
        } finally {
            return $report;
        }
    }
}