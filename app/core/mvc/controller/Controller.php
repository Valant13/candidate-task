<?php

namespace App\Core\Mvc\Controller;

abstract class Controller
{
    private static $_instance = array();

    public static function getInstance()
    {
        if (!array_key_exists(static::class, self::$_instance)) {
            self::$_instance[static::class] = new static();
        }
        return self::$_instance[static::class];
    }
}