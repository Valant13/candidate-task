<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class MigrationModel extends Model
{
    protected const logFilename = 'migration.log';

    public function migrateDatabase()
    {
        return App::getDatabase()->migrate();
    }

    public function logMigration($databaseConnectionMessage, $databaseConfigurationMessage, $tablesCreationMessage, $tablesCreationDetails)
    {
        App::getLog()->clear($this::logFilename);

        if (!is_null($databaseConnectionMessage)) App::getLog()->write($databaseConnectionMessage, $this::logFilename);
        if (!is_null($databaseConfigurationMessage)) App::getLog()->write($databaseConfigurationMessage, $this::logFilename);
        if (!is_null($tablesCreationMessage)) App::getLog()->write($tablesCreationMessage, $this::logFilename);

        if (!is_null($tablesCreationDetails)) {
            if (is_array($tablesCreationDetails)) {
                foreach ($tablesCreationDetails as $item) {
                    App::getLog()->write($item, $this::logFilename);
                }
            } else {
                App::getLog()->write($tablesCreationDetails, $this::logFilename);
            }
        }
    }

    public function validateReferer()
    {
        return App::getReferer()->validate();
    }
}