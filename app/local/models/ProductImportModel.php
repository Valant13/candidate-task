<?php

namespace App\Local\Models;

use App\App;
use App\Core\Mvc\Model\Model;

class ProductImportModel extends Model
{
    protected const imagesDirectory = 'media/productimages/';
    protected const imagesTypes = array('image/png', 'image/jpg', 'image/jpeg');
    protected const imagesMaxSize = 1000000;
    protected const imagesLogFilename = 'productimport/images.log';

    protected const productsType = 'xml';
    protected const productsMaxSize = 10000;
    protected const productsLogFilename = 'productimport/products.log';

    protected const schemaDirectory = 'var/';
    protected const schemaFilename = 'productImport.xsd';

    protected const skuPrefixesDirectory = 'var/';
    protected const skuPrefixesFilename = 'skuPrefixes.txt';
    protected const skuPrefixesDelimiter = ',';
    protected const semaphoreKey = 'productImport';
    protected $skuPrefixes;

    public function uploadImages()
    {
        return App::getFile()->uploadToDirectory('images', $this::imagesDirectory, function ($name, $type, $size, $tmpName) {
            if (!in_array($type, $this::imagesTypes)) throw new \Exception("File type is not available");
            if ($size > $this::imagesMaxSize) throw new \Exception("File is too big");
            return true;
        });
    }

    public function logImages($fileUploadingMessage, $fileUploadingDetails)
    {
        $filePaths = explode('.', $this::imagesLogFilename);
        $filePaths[0] = $filePaths[0] . date("_Y-m-d_H:i:s");
        $logFilename = implode('.', $filePaths);

        if (!is_null($fileUploadingMessage)) App::getLog()->write($fileUploadingMessage, $logFilename);

        if (!is_null($fileUploadingDetails)) {
            if (is_array($fileUploadingDetails)) {
                foreach ($fileUploadingDetails as $item) {
                    App::getLog()->write($item, $logFilename);
                }
            } else {
                App::getLog()->write($fileUploadingDetails, $logFilename);
            }
        }
    }

    public function uploadProducts()
    {
        $result = array();
        $result['fileReadingMessage'] = null;
        $result['productImportMessage'] = null;
        $result['productImportDetails'] = null;

        $report = $this->uploadAndReadProductsXml();
        $result['fileReadingMessage'] = $report->fileReadingMessage;
        if (!$report->fileReadingSuccess) return $result;

        $xml = $report->fileReadingContent;
        $xmlError = $this->validateXml($xml);
        if (!is_null($xmlError)) {
            $result['productImportMessage'] = "Products import failed. $xmlError";
            return $result;
        }

        $result['productImportMessage'] = "Products imported successfully";
        $result['productImportDetails'] = array();
        $productsXml = $this->splitProductsXml($xml);
        foreach ($productsXml as $key => $productXml) {
            $productXmlError = $this->validateProductXml($productXml);
            if (!is_null($productXmlError)) {
                $result['productImportMessage'] = "Import of some products failed";
                array_push($result['productImportDetails'], "Product " . ($key + 1) . " failed. $productXmlError");
                continue;
            }

            $product = $this->parseProductXml($productXml);
            $productError = $this->validateProduct($product);
            if (!is_null($productError)) {
                $result['productImportMessage'] = "Import of some products failed";
                array_push($result['productImportDetails'], "Product " . ($key + 1) . " failed. $productError");
                continue;
            }

            $this->importProduct($product);

            array_push($result['productImportDetails'], "Product " . ($key + 1) . " imported");
        }

        return $result;
    }

    protected function uploadAndReadProductsXml()
    {
        return App::getFile()->uploadAndRead('products', function ($name, $type, $size, $tmpName) {
            if ($type == $this::productsType) throw new \Exception("File type is not available");
            if ($size > $this::productsMaxSize) throw new \Exception("File is too big");
            return true;
        });
    }

    protected function validateXml($xml)
    {
        return App::getXml()->validate($xml);
    }

    protected function splitProductsXml($xml)
    {
        return App::getXml()->split($xml);
    }

    protected function validateProductXml($xml)
    {
        $xsd = App::getFile()->read($this::schemaDirectory . $this::schemaFilename);
        return App::getXml()->validate($xml, $xsd->fileReadingContent);
    }

    protected function parseProductXml($xml)
    {
        $productNode = App::getXml()->parse($xml);

        $product = array();

        $attributes = $productNode->attributes();
        foreach ($attributes as $key => $value)
            $product[$key] = (string)$value;
        if (!array_key_exists('likes', $product)) $product['likes'] = 0;
        $product['description'] = (string)($productNode->description);

        $product['discounts'] = array();
        $discountNodes = $productNode->discount;
        foreach ($discountNodes as $discountNode) {
            $discount = array();
            $discount['startsAt'] = date_format(date_create($discountNode->startsAt), 'Y-m-d H:i:s');
            $discount['endsAt'] = date_format(date_create($discountNode->endsAt), 'Y-m-d H:i:s');
            $discount['percent'] = (string)($discountNode->percent);
            array_push($product['discounts'], $discount);
        }

        $product['reviews'] = array();
        $reviewNodes = $productNode->review;
        foreach ($reviewNodes as $reviewNode) {
            $review = array();
            $review['date'] = date_format(date_create($reviewNode->date), 'Y-m-d H:i:s');
            $review['name'] = (string)($reviewNode->name);
            $review['text'] = (string)($reviewNode->text);
            array_push($product['reviews'], $review);
        }

        return $product;
    }

    protected function validateProduct($product)
    {
        $error = null;

        $discounts = $product['discounts'];
        $discountsSize = sizeof($discounts);
        for ($key1 = 0; $key1 < $discountsSize; $key1++) {
            for ($key2 = $key1 + 1; $key2 < $discountsSize; $key2++) {
                if ($discounts[$key1]['startsAt'] >= $discounts[$key1]['endsAt']) {
                    $error = "Discount " . ($key1 + 1) . " has incorrect period";
                    return $error;
                }

                if ($discounts[$key2]['startsAt'] >= $discounts[$key2]['endsAt']) {
                    $error = "Discount " . ($key2 + 1) . " has incorrect period";
                    return $error;
                }

                if ($discounts[$key1]['startsAt'] < $discounts[$key2]['startsAt'] &&
                    $discounts[$key1]['startsAt'] < $discounts[$key2]['endsAt'] &&
                    $discounts[$key1]['endsAt'] <= $discounts[$key2]['startsAt'] &&
                    $discounts[$key1]['endsAt'] < $discounts[$key2]['endsAt'])
                    continue;

                if ($discounts[$key1]['startsAt'] > $discounts[$key2]['startsAt'] &&
                    $discounts[$key1]['startsAt'] >= $discounts[$key2]['endsAt'] &&
                    $discounts[$key1]['endsAt'] > $discounts[$key2]['startsAt'] &&
                    $discounts[$key1]['endsAt'] > $discounts[$key2]['endsAt'])
                    continue;

                $error = "Discounts " . ($key1 + 1) . " and " . ($key2 + 1) . " have the same periods";
                return $error;
            }
        }

        $sku = $product['sku'];
        $skuPrefixes = $this->getSkuPrefixes();
        $flag = false;
        foreach ($skuPrefixes as $skuPrefix) {
            if (substr($sku, 0, strlen($skuPrefix)) == $skuPrefix) {
                $flag = true;
                if (strlen($sku) == strlen($skuPrefix)) $error = "Incorrect SKU";
                break;
            }
        }
        if (!$flag) $error = "SKU prefix is not available";

        return $error;
    }

    protected function getSkuPrefixes()
    {
        if (is_null($this->skuPrefixes)) {
            $report = App::getFile()->read($this::skuPrefixesDirectory . $this::skuPrefixesFilename);
            if (!$report->fileReadingSuccess) throw new \Exception("Unable to load sku prefixes. " . $report->fileReadingMessage);

            $content = $report->fileReadingContent;
            $this->skuPrefixes = explode($this::skuPrefixesDelimiter, $content);

            for ($key = 0; $key < sizeof($this->skuPrefixes); $key++)
                $this->skuPrefixes[$key] = trim($this->skuPrefixes[$key], "\n");
        }

        return $this->skuPrefixes;
    }

    protected function importProduct($product)
    {
        $id = App::getTable('products')->selectIdBySku($product['sku']);

        if ($id) {
            App::getTable('discounts')->deleteDiscountsByProductId($id);

            App::getTable('reviews')->deleteReviewsByProductId($id);

            App::getTable('products')->updateProductById(
                $id,
                $product['name'],
                $product['sku'],
                $product['image'],
                $product['description'],
                $product['price'],
                $product['likes']
            );
        } else {
            $id = App::getTable('products')->insertProduct(
                $product['name'],
                $product['sku'],
                $product['image'],
                $product['description'],
                $product['price'],
                $product['likes']
            );
        }

        foreach ($product['discounts'] as $discount) {
            App::getTable('discounts')->insertDiscount(
                $id,
                $discount['startsAt'],
                $discount['endsAt'],
                $discount['percent']
            );
        }

        foreach ($product['reviews'] as $review) {
            App::getTable('reviews')->insertReview(
                $id,
                $review['date'],
                $review['name'],
                $review['text']
            );
        }
    }

    public function logProducts($fileReadingMessage, $productImportMessage, $productImportDetails)
    {
        $filePaths = explode('.', $this::productsLogFilename);
        $filePaths[0] = $filePaths[0] . date("_Y-m-d_H:i:s");
        $logFilename = implode('.', $filePaths);

        if (!is_null($fileReadingMessage)) App::getLog()->write($fileReadingMessage, $logFilename);
        if (!is_null($productImportMessage)) App::getLog()->write($productImportMessage, $logFilename);

        if (!is_null($productImportDetails)) {
            foreach ($productImportDetails as $item) {
                App::getLog()->write($item, $logFilename);
            }
        }
    }

    public function getSemaphore()
    {
        App::getSemaphore()->get($this::semaphoreKey);
    }

    public function releaseSemaphore()
    {
        App::getSemaphore()->release($this::semaphoreKey);
    }

    public function validateReferer()
    {
        return App::getReferer()->validate();
    }
}