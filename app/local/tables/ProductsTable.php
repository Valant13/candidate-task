<?php

namespace App\Local\Tables;

use App\App;
use App\Core\Database\Table;

class ProductsTable extends Table
{
    public $migrationOrder = 0;

    public function create()
    {
        App::getDatabase()->execute("
            CREATE TABLE products (
                id_product INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
                name VARCHAR(64), 
                sku VARCHAR(16) UNIQUE, 
                image VARCHAR(64), 
                description TEXT, 
                price DECIMAL(8, 2) UNSIGNED,
                likes INT UNSIGNED DEFAULT 0
            );
            
            CREATE INDEX name ON products(name);
            
            CREATE INDEX price ON products(price);
        ");
    }

    public function selectProductsOrderedByNameAscLimited($limit)
    {
        $statement = App::getDatabase()->execute("
            SELECT products.id_product, name, price, TRUNCATE(price * (100 - percent) / 100, 2) AS discount_price, likes
            FROM products 
            LEFT JOIN (SELECT id_product, percent FROM discounts WHERE NOW() >= starts_at AND NOW() < ends_at) AS temp 
            ON products.id_product = temp.id_product
            ORDER BY name
            LIMIT " . App::getDatabase()->quote($limit, \PDO::PARAM_INT) . ";
        ");

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectProductsOrderedByNameDescLimited($limit)
    {
        $statement = App::getDatabase()->execute("
            SELECT products.id_product, name, price, TRUNCATE(price * (100 - percent) / 100, 2) AS discount_price, likes
            FROM products 
            LEFT JOIN (SELECT id_product, percent FROM discounts WHERE NOW() >= starts_at AND NOW() < ends_at) AS temp 
            ON products.id_product = temp.id_product
            ORDER BY name DESC
            LIMIT " . App::getDatabase()->quote($limit, \PDO::PARAM_INT) . ";
        ");

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectProductsOrderedByPriceAscLimited($limit)
    {
        $statement = App::getDatabase()->execute("
            SELECT products.id_product, name, price, TRUNCATE(price * (100 - percent) / 100, 2) AS discount_price, likes
            FROM products 
            LEFT JOIN (SELECT id_product, percent FROM discounts WHERE NOW() >= starts_at AND NOW() < ends_at) AS temp 
            ON products.id_product = temp.id_product
            ORDER BY price
            LIMIT " . App::getDatabase()->quote($limit, \PDO::PARAM_INT) . ";
        ");

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectProductsOrderedByPriceDescLimited($limit)
    {
        $statement = App::getDatabase()->execute("
            SELECT products.id_product, name, price, TRUNCATE(price * (100 - percent) / 100, 2) AS discount_price, likes
            FROM products 
            LEFT JOIN (SELECT id_product, percent FROM discounts WHERE NOW() >= starts_at AND NOW() < ends_at) AS temp 
            ON products.id_product = temp.id_product
            ORDER BY price DESC
            LIMIT " . App::getDatabase()->quote($limit, \PDO::PARAM_INT) . ";
        ");

        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectProductById($id)
    {
        $statement = App::getDatabase()->execute("
            SELECT name, sku, image, description, price, TRUNCATE(price * (100 - percent) / 100, 2) AS discount_price, likes
            FROM products 
            LEFT JOIN (SELECT id_product, percent FROM discounts WHERE NOW() >= starts_at AND NOW() < ends_at) AS temp 
            ON products.id_product = temp.id_product
            WHERE products.id_product = " . App::getDatabase()->quote($id) . ";
        ");

        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function updateLikesById($id)
    {
        $statement = App::getDatabase()->execute("UPDATE products SET likes = likes + 1 WHERE id_product = ?;", array($id));

        $result = boolval($statement->rowCount());
        return $result;
    }

    public function selectNameById($id)
    {
        $statement = App::getDatabase()->execute("SELECT name FROM products WHERE id_product = ?;", array($id));

        $result = $statement->fetch(\PDO::FETCH_ASSOC)['name'];
        return $result;
    }

    public function selectIdBySku($sku)
    {
        $statement = App::getDatabase()->execute("SELECT id_product FROM products WHERE sku = ?;", array($sku));

        $result = $statement->fetch(\PDO::FETCH_ASSOC)['id_product'];
        $result = (is_null($result) ? false : $result);
        return $result;
    }

    public function updateProductById($id, $name, $sku, $image, $description, $price, $likes)
    {
        $statement = App::getDatabase()->execute("
            UPDATE products SET name = ?, sku = ?, image = ?, description = ?, price = ?, likes = ?
            WHERE id_product = ?;
        ", array($name, $sku, $image, $description, $price, $likes, $id));

        $result = boolval($statement->rowCount());
        return $result;
    }

    public function insertProduct($name, $sku, $image, $description, $price, $likes)
    {
        $statement = App::getDatabase()->execute("INSERT INTO products VALUES (NULL, ?, ?, ?, ?, ?, ?);", array($name, $sku, $image, $description, $price, $likes));

        $result = ($statement->rowCount() ? App::getDatabase()->getLastInsertId() : 0);
        return $result;
    }

    public function selectExistsId($id)
    {
        $statement = App::getDatabase()->execute("SELECT EXISTS(SELECT * FROM products WHERE id_product = ?) AS result;", array($id));

        $result = $statement->fetch(\PDO::FETCH_ASSOC)['result'];
        $result = boolval($result);
        return $result;
    }
}