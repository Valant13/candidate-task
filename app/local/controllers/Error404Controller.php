<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;

class Error404Controller extends Controller
{
    public function indexAction()
    {
        App::getView()->setLayout('single');
        App::getView()->addTemplate('main', 'error404');
        App::getView()->display();
    }
}