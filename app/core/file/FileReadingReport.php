<?php

namespace App\Core\File;

class FileReadingReport
{
    public $fileReadingMessage;
    public $fileReadingSuccess;
    public $fileReadingContent;
}