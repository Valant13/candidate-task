<?php

namespace App\Core\Database;


class DatabaseMigrationReport
{
    public $databaseConnectionMessage;

    public $databaseConfigurationMessage;

    public $tablesCreationMessage;
    public $tablesCreationDetails;
    public $tablesCreationSuccess;
}