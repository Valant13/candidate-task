<?php

namespace App\Local\Controllers;

use App\App;
use App\Core\Mvc\Controller\Controller;
use App\Core\Mvc\Route;
use App\Core\Superglobals\Post;

class HomeController extends Controller
{
    public function indexAction()
    {
        $bestsellersHTML = App::getModel('bestsellers')->getBestsellersHTML();
        App::getModel('bestsellers')->processBestsellersHTML($bestsellersHTML);
        App::getTemplate('bestsellers')->addSafeData('content', $bestsellersHTML);

        $sortColumn = 'name';
        $sortOrder = 1;

        $products = App::getModel('productList')->getProducts($sortColumn, $sortOrder);

        $data = array();
        if ($products == false) {
            $data['state'] = 'error';
        } else {
            $data['state'] = 'success';
            $data['sort_column'] = $sortColumn;
            $data['sort_order'] = $sortOrder;
            $data['products'] = $products;
        }

        App::getTemplate('productList')->addJsonData('data', $data);

        App::getView()->setLayout('double');
        App::getView()->addTemplate('top', 'bestsellers');
        App::getView()->addTemplate('main', 'productList');
        App::getView()->display();
    }

    public function sortAction()
    {
        if (!App::getModel('productList')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Post::checkMultiple(['sort_column', 'sort_order'])) {
            App::getView()->answerAjax(array('state' => 'error'));
        } else {
            $sortColumn = Post::get('sort_column');
            $sortOrder = (int)Post::get('sort_order');

            $products = App::getModel('productList')->getProducts($sortColumn, $sortOrder);

            $data = array();
            if ($products == false) {
                $data['state'] = 'error';
            } else {
                $data['state'] = 'success';
                $data['sort_column'] = $sortColumn;
                $data['sort_order'] = $sortOrder;
                $data['products'] = $products;
            }

            App::getView()->answerAjax($data);
        }
    }

    public function likeAction()
    {
        if (!App::getModel('productList')->validateReferer()) {
            Route::redirectError404();
            return;
        }

        if (!Post::check('id')) {
            App::getView()->answerAjax(array('state' => 'error'));
        } else {
            $id = Post::get('id');
            $success = App::getModel('productList')->likeProduct($id);

            $data = array();
            if ($success)
                $data['state'] = 'success';
            else
                $data['state'] = 'error';
            App::getView()->answerAjax($data);
        }
    }
}