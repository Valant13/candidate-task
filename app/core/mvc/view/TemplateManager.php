<?php

namespace App\Core\Mvc\View;

class TemplateManager
{
    protected const templateDirectory = 'app/local/views/templates/';

    protected static $templates = array();

    public static function getTemplate($templateName): Template
    {
        if (!array_key_exists($templateName, self::$templates)) {
            $templateFile = self::templateDirectory . ucfirst($templateName) . 'Template' . '.php';
            if (!is_file($templateFile)) {
                throw new \Exception("File `$templateFile` not found");
            }
            self::$templates[$templateName] = new Template($templateFile);
        }
        return self::$templates[$templateName];
    }
}